import Vue from "vue";
import App from "./App.vue";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faUserCircle, faHome, faBell, faChartBar, faInfoCircle, faClipboardList, faComments, faRunning, faClipboard} from "@fortawesome/free-solid-svg-icons";
import VueRouter from "vue-router";
import Home from "./components/Home.vue";
import FormDaftar from "./components/FormDaftar.vue";


library.add(faUserCircle, faHome, faBell, faChartBar, faInfoCircle, faClipboardList, faComments, faRunning, faClipboard);

Vue.use(VueRouter);
Vue.config.productionTip = false;

const router = new VueRouter({
  routes: [
    {
      path: "*",
      component: Home
    },
    {
      path: "/daftar",
      component: FormDaftar
    }
  ]
});

new Vue({
  render: (h) => h(App),
  router
}).$mount("#app");
